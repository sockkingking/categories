package com.kaopiz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CategoriesController {

	@GetMapping("/list-categories")
	public String showList() {
		return "showList";
	}
}
