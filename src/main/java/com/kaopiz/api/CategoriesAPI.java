package com.kaopiz.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kaopiz.entity.CategoriesEntity;
import com.kaopiz.service.CategoriesService;

@RestController
public class CategoriesAPI {

	@Autowired
	private CategoriesService categoriesService;

	@GetMapping("/api/list-categories")
	public List<CategoriesEntity> getAll() {

		return categoriesService.getList();
	}

	@GetMapping("/api/list-select")
	public List<CategoriesEntity> getListSelect() {

		return categoriesService.getListSelect();
	}

	@GetMapping("/api/detail")
	public CategoriesEntity getListSelect(@RequestParam("id") Long id) {

		return categoriesService.detail(id);
	}

	@GetMapping("/api/add-categories")
	public boolean addCategories(@RequestParam("name") String name, @RequestParam("line") String line,
			@RequestParam("idUpdate") String idUpdate) {
		return categoriesService.addCate(name, line, idUpdate);
	}

	@GetMapping("/api/delete-categories")
	public boolean deleteCate(@RequestParam("id") Long id) {
		return categoriesService.deleteCate(id);
	}

}
