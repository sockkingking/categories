package com.kaopiz.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "categories")
public class CategoriesEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCate;
	private String name;
	private String level;
	private String line;
	public Long getIdCate() {
		return idCate;
	}
	public void setIdCate(Long idCate) {
		this.idCate = idCate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public CategoriesEntity( String name, String level, String line) {
		super();
		
		this.name = name;
		this.level = level;
		this.line = line;
	}
	public CategoriesEntity() {
		super();
	}
	
	
	
}
