package com.kaopiz.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kaopiz.entity.CategoriesEntity;
import com.kaopiz.repository.CategoriesRepository;

@Service
public class CategoriesService {

	@Autowired
	private CategoriesRepository categoriesRepository;

	public List<CategoriesEntity> getList() {
		List<CategoriesEntity> middleList = categoriesRepository.findAllByOrderByLineAsc();
		for (CategoriesEntity categoris : middleList) {
			if (Integer.valueOf(categoris.getLevel()) > 1) {

				String subString = "";
				for (int i = 0; i < Integer.valueOf(categoris.getLevel()) - 1; i++) {
					subString += "|----";
				}
				String name = subString + categoris.getName();
				categoris.setName(name);
			}
		}

		return middleList;
	}

	public List<CategoriesEntity> getListSelect() {
		return categoriesRepository.findAll();
	}

	public boolean addCate(String name, String line, String idUpdate) {
		if (Integer.valueOf(idUpdate) == 0) {
			return addCategories(name, line);
		} else {
			return updateCategories(name, line, idUpdate);
		}
	}

	private boolean updateCategories(String name, String line, String idUpdate) {
		System.out.println(line);
		if (!categoriesRepository.findById(Long.valueOf(idUpdate)).isPresent()) {
			return false;
		}

		String s[] = line.split("/");
		CategoriesEntity categoriesEntity = categoriesRepository.findById(Long.valueOf(idUpdate)).get();
		String oldLine = categoriesEntity.getLine();
		categoriesEntity.setName(name);
		
		if(s.length == 0) {
			categoriesEntity.setLevel("1");
		}
		else {
			categoriesEntity.setLevel(String.valueOf(s.length + 1));
		}
		String newLine = line + idUpdate + "/";
		categoriesEntity.setLine(newLine);
		
		categoriesRepository.save(categoriesEntity);
		
		List<CategoriesEntity> categoriesEntities = categoriesRepository.getListByLine(oldLine + "%");
		for (CategoriesEntity i : categoriesEntities) {
			String m = i.getLine();
			i.setLevel(String.valueOf(s.length + 2));
			i.setLine(newLine + i.getIdCate());
		}
		categoriesRepository.saveAll(categoriesEntities);
		return true;
	}

	private boolean addCategories(String name, String line) {
		String[] s = line.split("/");
		CategoriesEntity categoriesEntity = new CategoriesEntity();
		categoriesEntity.setName(name);

		if (Integer.valueOf(s[0]) == 0) {
			categoriesEntity.setLevel("1");
			categoriesRepository.save(categoriesEntity);
			categoriesEntity.setLine(categoriesEntity.getIdCate() + "/");
		} else {
			categoriesEntity.setLevel(String.valueOf(s.length + 1));
			categoriesRepository.save(categoriesEntity);
			categoriesEntity.setLine(line + categoriesEntity.getIdCate() + "/");
		}

		categoriesRepository.save(categoriesEntity);
		return true;
	}

	public CategoriesEntity detail(Long id) {
		/*
		 * CategoriesEntity categoriesEntity; try { categoriesEntity =
		 * categoriesRepository.findById(id).get(); } catch (Exception e) { return null;
		 * } String temp = ""; String s[] = categoriesEntity.getLine().split("/");
		 * for(String i : s) { if(Long.valueOf(i) != categoriesEntity.getIdCate()) {
		 * temp += i + "/"; }else { break; } } categoriesEntity.setLine(temp);
		 */
		Optional<CategoriesEntity> categoriesEntity = categoriesRepository.findById(id);

		if (!categoriesEntity.isPresent()) {
			return null;
		}
		return categoriesEntity.get();
	}

	public boolean deleteCate(Long id) {
		Optional<CategoriesEntity> categoriesEntity = categoriesRepository.findById(id);

		if (!categoriesEntity.isEmpty()) {
			String oldLine = categoriesEntity.get().getLine();;
//			String s [] = oldLine.split("/");
			List<CategoriesEntity> categoriesEntities = categoriesRepository.getListByLine(oldLine + "%");
			for (CategoriesEntity i : categoriesEntities) {
				String m = i.getLine();
				i.setLevel(String.valueOf(Integer.valueOf(i.getLevel())-1));
				i.setLine(i.getLine().substring(oldLine.length(), i.getLine().length()));
			}
			categoriesRepository.saveAll(categoriesEntities);
			categoriesRepository.deleteById(id);
			
			
			
			return true;
		}

		return false;
	}

}
