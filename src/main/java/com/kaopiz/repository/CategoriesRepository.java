package com.kaopiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kaopiz.entity.CategoriesEntity;

@Repository
public interface CategoriesRepository extends JpaRepository<CategoriesEntity, Long>{

	List<CategoriesEntity> findAllByOrderByLineAsc();
	
	CategoriesEntity findByLine(String line);
	
	@Query("from CategoriesEntity c where c.line like ?1")
	List<CategoriesEntity> getListByLine(String line);

	
	
	
}
