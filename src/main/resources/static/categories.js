$(document).ready(function() {
	var idUpdate;
	
	$.ajax({
		type : "GET",
		contentType : "application/json",
		url : '/api/list-select',
		data : "",
		error : function(xhr, ajaxOptions, thrownError) {
		},
		success : function(response) {
			var text = "<option value='0'>None</option>";
			for(var i = 0; i < response.length; i++){
				text += "<option value='"+response[i].line+"'>"+response[i].name+"</option>";
			}
			$("#sel1").html(text);
		}
	});
	
	 var columnDefinitions = [
	    	{"data":"idCate","class": "text-left", "orderable": false},
	    	{"data":"name","class": "text-left"},
	        {"data": "level", "defaultContent": "","orderable": false}, 
	        {"data": "line","class": "text-left", "orderable": true },
	        {"data": null, "class": "text-left","orderable": false }
	        
	    ];

	   var dataTable = $('#content_table').DataTable({
	        "ordering": true,
	        "serverSide": true,
	        "order": [1, "asc"],
	        "searching": false,
	        "ajax": 'json',
	        "ajax": {
	            url: "/api/list-categories",
	            type: 'GET',
	            dataSrc:""
	        },
	        "columns": columnDefinitions,
	        "lengthMenu": [
	            [10, 20, 50],
	            [10, 20, 50]
	        ],
	        columnDefs: [
	        	{
	        		"render": function (data) {
	                    return "<button class='btn btn-danger btn-delete' id= 'delete_"+data.idCate+"' style='margin-right: 5px;'>Delete</button>" 
	                    + "<button class='btn btn-primary btn-edit' id = 'edit_"+data.idCate+"' data-toggle='modal' data-target='#addModal'>Edit</button>" ;
	                },
	                "targets": 4
	        	}
	        ]

	    });
	$("#content_table_info").hide();
	
	$("#add").on("click", function() {
		idUpdate = 0;
		reloadData();
	})
	
	$(document).on("click", ".btn-delete", function(){
		let id = $(this).attr("id").replace("delete_", "");
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : '/api/delete-categories?id='+id,
			data : "",
			error : function(xhr, ajaxOptions, thrownError) {
			},
			success : function(response) {
				if(response == true){
					dataTable.ajax.reload();
					alert("Delete Success!");
				}else{
					alert("Delete Fail!");
				}
			  }
			});
	})
	
	function reloadData(){
		$("#name_cate").val("");
		$("#sel1").val("");
	}
	
	$(document).on("click", "#add_cate", function() {
		var name = $("#name_cate").val();
		var line = $("#sel1").val();
		console.log(line);
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : '/api/add-categories?name='+name+"&line="+line+"&idUpdate="+idUpdate,
			data : "",
			error : function(xhr, ajaxOptions, thrownError) {
			},
			success : function(response) {
				if(response == true){
					alert("Add Success!");
					window.location.reload();
				}else{
					alert("Add Fail!");
				}
			}
		});
	})
	
	$(document).on("click", ".btn-edit", function() {
		idUpdate = $(this).attr("id").replace("edit_", "");
		console.log(idUpdate);
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : '/api/detail?id='+idUpdate,
			data : "",
			error : function(xhr, ajaxOptions, thrownError) {
			},
			success : function(response) {
				$("#name_cate").val(response.name);
				$("select#sel1").val(response.line);
			}
		});
	})
})